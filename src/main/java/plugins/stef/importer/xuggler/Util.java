package plugins.stef.importer.xuggler;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.HashSet;
import java.util.Set;

import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;

import icy.common.exception.UnsupportedFormatException;
import icy.file.FileUtil;
import icy.image.IcyBufferedImage;
import icy.type.collection.CollectionUtil;

/**
 * Utilities class for the video importer plugin.
 *
 * @author Stephane Dallongeville
 */
public class Util {
    private final static Set<String> supportedExtensions = new HashSet<String>(
            CollectionUtil.asList(new String[]{"avi", "mp4", "mov", "wmv", "mpg", "mpeg"}));
    private final static Set<String> unsupportedExtensions = new HashSet<String>(
            CollectionUtil.asList(new String[]{"xml", "txt", "pdf", "xls", "xlsx", "doc", "docx", "pdf", "rtf", "exe",
                    "wav", "mp3", "app", "lsm", "bmp", "png"}));

    /**
     * @return the stream index for the video stream (<code>-1</code> if no video stream found)
     * @param container IContainer
     */
    public static int getVideoStreamIndex(IContainer container) {
        for (int i = 0; i < container.getNumStreams(); i++) {
            final IStreamCoder streamCoder = container.getStream(i).getStreamCoder();

            if (streamCoder != null) {
                final ICodec codec = streamCoder.getCodec();

                if (codec != null) {
                    if (codec.getType() == ICodec.Type.CODEC_TYPE_VIDEO)
                        return i;
                }
            }
        }

        return -1;
    }

    /**
     * @return the time interval in second (interval between 2 frames).
     * @param stream IStream
     * @param streamCoder IStreamCoder
     */
    public static double getTimeInterval(IStream stream, IStreamCoder streamCoder) {
        double result;

        // result = streamCoder.getTimeBase().getValue();
        // if (result > 0d)
        // return result;

        result = streamCoder.getFrameRate().getValue();
        if (result > 0d)
            return 1d / result;

        result = stream.getFrameRate().getValue();
        if (result > 0d)
            return 1d / result;

        return 0d;
    }

    /**
     * @return an {@link IcyBufferedImage} corresponding to the first channel of the specified RGB {@link BufferedImage}
     * @param image BufferedImage
     */
    public static IcyBufferedImage getFirstChannelImage(BufferedImage image) {
        final int w = image.getWidth();
        final int h = image.getHeight();
        final byte[] data = new byte[w * h];
        final byte[] srcData = ((DataBufferByte) image.getRaster().getDataBuffer()).getData(0);

        for (int i = 0; i < data.length; i++)
            data[i] = srcData[i * 3];

        return new IcyBufferedImage(w, h, data, false, true);
    }

    /**
     * @return Open the specified file and returns the {@link IContainer} object to explore it.
     * @param path String
     * @throws UnsupportedFormatException Unsupported file format
     */
    public static IContainer open(String path) throws UnsupportedFormatException {
        // create a new Xuggler container object
        final IContainer container = IContainer.make();

        // try to open up the container
        if (container.open(FileUtil.getGenericPath(path), IContainer.Type.READ, null, false, false) < 0) {
            container.delete();
            throw new UnsupportedFormatException(path + " file format is not supported by Xuggler.");
        }

        return container;
    }

    /**
     * @return <code>true</code> if the specified file is supported for "open video" operation by Xuggler
     * @param filename String
     */
    public static boolean canOpenFile(String filename) {
        if (!FileUtil.exists(filename) || FileUtil.isDirectory(filename))
            return false;

        // get file extension
        final String ext = FileUtil.getFileExtension(filename, false).toLowerCase();

        // test against classic video extensions
        if (supportedExtensions.contains(ext))
            return true;
        // automatically discard some files from their extension.
        // for instance LSM can sometime be opened by Xuggler but then it fails later so avoid to spent time in that
        if (unsupportedExtensions.contains(ext))
            return false;

        try {
            // retrieve the Xuggler container object from specified file
            final IContainer container = open(filename);

            try {
                // we only want video stream
                return (container.getBitRate() > 0) && (getVideoStreamIndex(container) != -1);
            }
            finally {
                container.close();
                container.delete();
            }
        }
        catch (UnsupportedFormatException e) {
            return false;
        }
    }
}
