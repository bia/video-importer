package plugins.stef.importer.xuggler;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileFilter;

import com.xuggle.xuggler.Global;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.IVideoResampler;
import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.video.IConverter;

import icy.common.exception.UnsupportedFormatException;
import icy.file.FileUtil;
import icy.image.AbstractImageProvider;
import icy.image.IcyBufferedImage;
import icy.image.IcyBufferedImageUtil;
import icy.image.IcyBufferedImageUtil.FilterType;
import icy.image.ImageUtil;
import icy.plugin.abstract_.PluginSequenceFileImporter;
import icy.sequence.MetaDataUtil;
import icy.type.DataType;
import icy.util.OMEUtil;
import icy.util.StringUtil;
import loci.formats.gui.ExtensionFileFilter;
import loci.formats.ome.OMEXMLMetadataImpl;
import ome.xml.meta.OMEXMLMetadata;
import plugins.stef.library.XugglerPlugin;

/**
 * Video importer plugin for Icy using Xuggler library.
 * 
 * @author Stephane Dallongeville
 */
public class VideoImporter extends PluginSequenceFileImporter
{
    /**
     * Allow the importer to quickly parse the video file to get an estimation of the number of
     * frame for metadata.<br>
     * Note that when an image will be opened the correct number of frame will be computed anyway.
     * 
     * @deprecated Removed this feature
     */
    @Deprecated
    public static final int FAST_METADATA = 1 << 0;

    protected IContainer container;
    protected IStream stream;
    protected List<FrameIndex> frameIndexes;
    protected IStreamCoder videoCoder;
    protected IPacket decodePacket;
    protected IVideoResampler resampler;
    protected IConverter converter;
    protected int streamInd;
    protected int numFrame;
    protected int numChannel;
    protected long lastDecodedFrame;
    protected long lastTimeStamp;

    public VideoImporter()
    {
        super();

        // force native library loading
        XugglerPlugin.init();
        // disable Xuggler logging
        Global.setFFmpegLoggingLevel(-1);

        container = null;
        stream = null;
        frameIndexes = null;
        videoCoder = null;
        decodePacket = null;
        resampler = null;
        converter = null;
        // video stream index
        streamInd = -1;
        numFrame = 0;
        numChannel = 0;
        lastDecodedFrame = -1;
        lastTimeStamp = 0;
    }

    // /**
    // * Fix the specified frame index list.
    // */
    // protected List<IIndexEntry> fixIndexes(List<IIndexEntry> indexList)
    // {
    // final List<IIndexEntry> result = new ArrayList<IIndexEntry>();
    // long lastPos;
    // int lastKeyOffset;
    //
    // lastPos = -1;
    // lastKeyOffset = 0;
    // for (IIndexEntry entry : indexList)
    // {
    // final long currentPos = entry.getTimeStamp();
    //
    // // on same timestamp --> continue
    // if (currentPos == lastPos)
    // continue;
    //
    // // increment frame
    // lastPos++;
    //
    // // first frame ?
    // if (lastPos == 0)
    // {
    // // special case of missing first frames
    // while (lastPos < currentPos)
    // {
    // // duplicate first frames
    // result.add(IIndexEntry.make(entry.getPosition(), currentPos, 0, 0, 0));
    // lastPos++;
    // }
    // }
    // else
    // {
    // final long frameInd = lastPos - 1;
    // final int keyDist = lastKeyOffset;
    //
    // // insert missing frame indexes
    // while (lastPos < currentPos)
    // {
    // // duplicate last frame
    // result.add(IIndexEntry.make(entry.getPosition(), frameInd, 0, 0, keyDist));
    // lastKeyOffset++;
    // lastPos++;
    // }
    // }
    //
    // final int flag;
    //
    // if (entry.isKeyFrame())
    // {
    // lastKeyOffset = 0;
    // flag = IIndexEntry.IINDEX_FLAG_KEYFRAME;
    // }
    // else
    // flag = 0;
    //
    // result.add(IIndexEntry.make(entry.getPosition(), currentPos, flag, entry.getSize(),
    // lastKeyOffset));
    // lastKeyOffset++;
    // }
    //
    // final IIndexEntry lastEntry = indexList.get(indexList.size() - 1);
    //
    // // add missing last frame if needed
    // while (result.size() < stream.getNumFrames())
    // {
    // // duplicate last frame
    // result.add(IIndexEntry.make(lastEntry.getPosition() + lastEntry.getSize(), lastPos, 0, 0,
    // lastKeyOffset));
    // }
    //
    // return result;
    // }

    /**
     * Construct the frame indexes
     * 
     * @throws IOException
     *         IOException
     */
    protected void buildIndexes() throws IOException
    {
        final List<FrameIndex> result = new ArrayList<FrameIndex>();
        final IPacket packet = IPacket.make();
        // allocate picture
        IVideoPicture picture = IVideoPicture.make(videoCoder.getPixelType(), videoCoder.getWidth(),
                videoCoder.getHeight());
        int frame;
        long lastPos;
        long lastKeyFrame;

        frame = 0;
        lastPos = -1;
        lastKeyFrame = 0;
        while (container.readNextPacket(packet) >= 0)
        {
            if (packet.isComplete() && (packet.getStreamIndex() == streamInd))
            {
                final long currentPos = Math.max(0, packet.getTimeStamp());

                if (lastPos == -1)
                    lastPos = currentPos;

                if (packet.isKey())
                    lastKeyFrame = currentPos;

                int offset = 0;
                while (offset < packet.getSize())
                {
                    // decode
                    final int bytesDecoded = videoCoder.decodeVideo(picture, packet, offset);

                    if (bytesDecoded < 0)
                    {
                        System.out.println("Warning: can't decode packet " + packet.getTimeStamp());
                        break;
                    }

                    offset += bytesDecoded;

                    // done ?
                    if (picture.isComplete())
                    {
                        result.add(new FrameIndex(frame, lastPos, currentPos, lastKeyFrame));
                        lastPos = currentPos + 1;
                        frame++;

                        // delete picture and create a new one
                        picture.delete();
                        picture = IVideoPicture.make(videoCoder.getPixelType(), videoCoder.getWidth(),
                                videoCoder.getHeight());
                    }
                }
            }
        }

        picture.delete();
        packet.delete();

        // set frame indexes
        frameIndexes = result;
        // set number of frame
        numFrame = result.size();
        // save last time stamp
        lastTimeStamp = result.get(result.size() - 1).endStamp;
    }

    /**
     * Seek to specified timestamp.
     * 
     * @return <code>false</code> if there is no open file
     * @param timestamp
     *        long
     * @throws IOException
     *         IOException
     */
    public synchronized boolean seek(long timestamp) throws IOException
    {
        if (getOpened() == null)
            return false;

        // close the video coder
        videoCoder.close();
        // re-open the video coder
        if (videoCoder.open(null, null) < 0)
            throw new IOException("Could not open video stream of " + getOpened());

        // seek to desired timestamp
        // if (container.seekKeyFrame(streamInd, timestamp, IURLProtocolHandler.SEEK_SET) < 0)
        if (container.seekKeyFrame(streamInd, 0, timestamp, lastTimeStamp, 0) < 0)
            throw new IOException("Can't seek to frame '" + timestamp + "'  in file: " + getOpened());

        return true;
    }

    public synchronized boolean isOpen(String path)
    {
        return (!StringUtil.isEmpty(path)) && StringUtil.equals(getOpened(), FileUtil.getGenericPath(path));
    }

    @Override
    public synchronized String getOpened()
    {
        if ((container != null) && container.isOpened())
            return FileUtil.getGenericPath(container.getURL());

        return null;
    }

    @Override
    public synchronized boolean open(String path, int flags) throws UnsupportedFormatException, IOException
    {
        if (StringUtil.isEmpty(path))
            throw new IOException("Empty path !");

        // already opened ?
        if (isOpen(path))
            return true;

        // close first
        close();

        // open the file
        final IContainer result = Util.open(path);

        // we want a video file
        if (result.getBitRate() <= 0)
        {
            result.close();
            result.delete();
            throw new UnsupportedFormatException("No video stream found in: " + getOpened());
        }

        // retrieve video stream index
        streamInd = Util.getVideoStreamIndex(result);
        // not found ?
        if (streamInd == -1)
        {
            result.close();
            result.delete();
            throw new UnsupportedFormatException("No video stream found in: " + getOpened());
        }

        // set container
        container = result;

        // get stream coder
        stream = container.getStream(streamInd);

        final IStreamCoder streamCoder = stream.getStreamCoder();

        // try to open the decoder so it can do work
        if (streamCoder.open(null, null) < 0)
        {
            close();
            throw new IOException("Could not open video stream of " + getOpened());
        }

        if (streamCoder.getPixelType() != IPixelFormat.Type.BGR24)
        {
            // not in BGR24 ? --> need resampler to change color space
            resampler = IVideoResampler.make(streamCoder.getWidth(), streamCoder.getHeight(), IPixelFormat.Type.BGR24,
                    streamCoder.getWidth(), streamCoder.getHeight(), streamCoder.getPixelType());
            if (resampler == null)
            {
                close();
                throw new UnsupportedFormatException("Could not create color space resampler for: " + getOpened());
            }
        }
        else
            resampler = null;

        converter = ConverterFactory.createConverter(ConverterFactory.XUGGLER_BGR_24, IPixelFormat.Type.BGR24,
                streamCoder.getWidth(), streamCoder.getHeight());
        if (converter == null)
        {
            close();
            throw new UnsupportedFormatException("Could not create image converter for: " + getOpened());
        }

        videoCoder = streamCoder;

        // build frame indexes (no way to get accurate indexes nor number of frame without that)
        buildIndexes();

        // try to determine the number of channel (default = RGB = 3)
        numChannel = 3;
        final int step = numFrame / 5;

        // sufficient number of image ?
        if (step > 0)
        {
            boolean gray = true;
            int i = 1;
            while (gray && (i < 5))
            {
                gray &= ImageUtil.isGray(getBufferedImage(i * step));
                i++;
            }

            // gray image --> single channel
            if (gray)
                numChannel = 1;
        }

        return true;
    }

    @Override
    public boolean acceptFile(String path)
    {
        return Util.canOpenFile(path);
    }

    @Override
    public List<FileFilter> getFileFilters()
    {
        final List<FileFilter> result = new ArrayList<FileFilter>();

        result.add(XugglerAllFileFilter.instance);
        result.add(new ExtensionFileFilter(new String[] {"avi"}, "AVI video / Xuggler"));
        result.add(new ExtensionFileFilter(new String[] {"mp4"}, "MPEG-4 video / Xuggler"));
        result.add(new ExtensionFileFilter(new String[] {"mov"}, "Quicktime Movie / Xuggler"));
        result.add(new ExtensionFileFilter(new String[] {"wmv"}, "Windows Media Video / Xuggler"));
        result.add(new ExtensionFileFilter(new String[] {"mpg", "mpeg"}, "MPEG video / Xuggler"));

        return result;
    }

    @Override
    public synchronized void close() throws IOException
    {
        // accessing videoCoder or container can make Xuggle crash here
        // so we prefer to just not close them (delete operation should be enough to release resources)
        // if ((videoCoder != null) && videoCoder.isOpen())
        // videoCoder.close();
        // if ((container != null) && container.isOpened())
        // container.close();

        if (resampler != null)
        {
            resampler.delete();
            resampler = null;
        }

        if (converter != null)
        {
            converter.delete();
            converter = null;
        }

        if (videoCoder != null)
        {
            videoCoder.delete();
            videoCoder = null;
        }

        if (decodePacket != null)
        {
            decodePacket.delete();
            decodePacket = null;
        }

        if (stream != null)
        {
            stream.delete();
            stream = null;
        }

        if (container != null)
        {
            container.delete();
            container = null;
        }

        streamInd = -1;
        numFrame = 0;
        numChannel = 0;
        lastDecodedFrame = -1;
        lastTimeStamp = 0;
        frameIndexes = null;
    }

    @Override
    public synchronized OMEXMLMetadata getOMEXMLMetaData() throws UnsupportedFormatException, IOException
    {
        // no image currently opened
        if (getOpened() == null)
            return null;

        final OMEXMLMetadata result = OMEUtil.createOMEXMLMetadata();

        MetaDataUtil.setNumSeries(result, 1);

        // System.out.println("duration in micro second: " + container.getDuration());
        // System.out.println("start time: " + container.getStartTime());
        //
        // System.out.println("start time (2): " + stream.getStartTime());
        // System.out.println("stream duration (2): " + stream.getDuration());
        // System.out.println("stream frame rate (2): " + stream.getFrameRate());
        // System.out.println("stream num frame : " + stream.getNumFrames());
        // System.out.println("stream num index : " + stream.getNumIndexEntries());
        // System.out.println("stream frame rate : " + stream.getFrameRate());
        // System.out.println("stream time base : " + stream.getTimeBase());

        MetaDataUtil.setDataType(result, 0, DataType.UBYTE);
        MetaDataUtil.setSizeX(result, 0, videoCoder.getWidth());
        MetaDataUtil.setSizeY(result, 0, videoCoder.getHeight());
        MetaDataUtil.setSizeC(result, 0, numChannel);
        MetaDataUtil.setSizeZ(result, 0, 1);
        MetaDataUtil.setSizeT(result, 0, numFrame);

        // if (numFrame > 0)
        // MetaDataUtil.setSizeT(result, 0, numFrame);
        // else
        // MetaDataUtil.setSizeT(result, 0, (int) stream.getNumFrames());

        final double timeInterval = Util.getTimeInterval(stream, videoCoder);

        // set time interval information
        MetaDataUtil.setTimeInterval(result, 0, (timeInterval * stream.getNumFrames()) / numFrame);

        return result;
    }

    @Deprecated
    @Override
    public OMEXMLMetadataImpl getMetaData() throws UnsupportedFormatException, IOException
    {
        return (OMEXMLMetadataImpl) getOMEXMLMetaData();
    }

    /**
     * Down scale the specified image with the given down scale factor.<br>
     * If down scale factor equals <code>0</code> then the input image is directly returned.
     * 
     * @param source
     *        input image
     * @param downScaleLevel
     *        scale factor
     * @return scaled image or source image is scale factor equals <code>0</code>
     */
    protected static IcyBufferedImage downScale(IcyBufferedImage source, int downScaleLevel)
    {
        IcyBufferedImage result = source;
        int it = downScaleLevel;

        // process fast down scaling
        while (it-- > 0)
            result = IcyBufferedImageUtil.downscaleBy2(result, true);

        return result;

        // final double scale = Math.pow(2, downScaleLevel);
        // if (scale > 1d)
        // {
        // final int sizeX = (int) (Math.round(source.getSizeX() / scale));
        // final int sizeY = (int) (Math.round(source.getSizeY() / scale));
        // // down scale
        // return IcyBufferedImageUtil.scale(source, sizeX, sizeY, FilterType.BILINEAR);
        // }
        //
        // return source;
    }

    protected IVideoPicture getPicture(FrameIndex index) throws IOException
    {
        // get corresponding timestamp (can be different in case of duplicated frame)
        final long frame = index.startStamp;
        // get corresponding key frame
        final long keyFrame = index.keyStamp;

        boolean doSeek = true;

        // packet not yet read ? --> create
        if (decodePacket == null)
            decodePacket = IPacket.make();

        // use last position if possible
        if (lastDecodedFrame != -1)
        {
            // next frame ? --> don't use seek
            if ((lastDecodedFrame + 1) == frame)
                doSeek = false;
            // last position between key frame and wanted frame --> don't use seek
            else if ((lastDecodedFrame < frame) && (lastDecodedFrame >= keyFrame))
                doSeek = false;
        }

        // seek if needed
        if (doSeek)
            seek(keyFrame);

        // allocate picture
        IVideoPicture picture = IVideoPicture.make(videoCoder.getPixelType(), videoCoder.getWidth(),
                videoCoder.getHeight());

        while (container.readNextPacket(decodePacket) >= 0)
        {
            if (decodePacket.isComplete() && (decodePacket.getStreamIndex() == streamInd))
            {
                int offset = 0;
                while (offset < decodePacket.getSize())
                {
                    // decode
                    final int bytesDecoded = videoCoder.decodeVideo(picture, decodePacket, offset);

                    if (bytesDecoded < 0)
                    {
                        System.out.println("Warning: can't decode packet " + decodePacket.getTimeStamp());
                        break;
                    }

                    offset += bytesDecoded;

                    // done ?
                    if (picture.isComplete())
                    {
                        // is it the good frame ? --> return it
                        if (decodePacket.getTimeStamp() >= frame)
                        {
                            // keep trace of last decoded frame
                            lastDecodedFrame = decodePacket.getTimeStamp();
                            return picture;
                        }

                        // delete picture and create a new one
                        picture.delete();
                        picture = IVideoPicture.make(videoCoder.getPixelType(), videoCoder.getWidth(),
                                videoCoder.getHeight());
                    }
                }
            }
        }

        // // special case where we didn't get the picture and we reached the end of the stream
        // if ((decodePacket.getTimeStamp() >= lastTimeStamp))
        // {
        // // get back to origin, sometime we need to fetch extra packet to complete the picture decode
        // seek(0);
        // extraPacket = true;
        // }

        picture.delete();

        return null;
    }

    protected IVideoPicture getPicture(int t) throws IOException
    {
        if (getOpened() == null)
            return null;

        // outside of frame range
        if (t >= frameIndexes.size())
            return null;

        IVideoPicture result = getPicture(frameIndexes.get(t));

        if (result == null)
        {
            int pos = t;
            int offset = 1;

            // can't retrieve the picture from this index ? --> try from previous (it can help)
            while ((result == null) && (offset < 10) && ((pos - offset) >= 0))
            {
                result = getPicture(frameIndexes.get(pos - offset));
                offset += 2;
            }

            if (result == null)
                throw new IOException("Error while retrieving image at frame " + t + " of " + getOpened());
        }

        return result;
    }

    /**
     * @return Return the image at <code>t</code> position in {@link BufferedImage#TYPE_3BYTE_BGR} format
     * @param t
     *        int
     * @throws IOException
     *         IOException
     */
    public synchronized BufferedImage getBufferedImage(int t) throws IOException
    {
        final IVideoPicture picture = getPicture(t);

        if (picture == null)
            return null;

        final IVideoPicture newPic;

        // need color space conversion ?
        if (resampler != null)
        {
            newPic = IVideoPicture.make(resampler.getOutputPixelFormat(), picture.getWidth(), picture.getHeight());
            if (resampler.resample(newPic, picture) < 0)
                throw new RuntimeException("Could not resample video (" + getOpened() + ")");
        }
        else
            newPic = picture;

        if (newPic.getPixelType() != IPixelFormat.Type.BGR24)
            throw new RuntimeException("Could not decode video as BGR 24 bit data (" + getOpened() + ")");

        final BufferedImage result = converter.toImage(newPic);

        // we can delete picture objects now
        if (newPic != picture)
            newPic.delete();
        picture.delete();

        return result;
    }

    @Override
    public IcyBufferedImage getImage(int z, int t) throws UnsupportedFormatException, IOException
    {
        // Z not supported for this importer
        if (z != 0)
            return null;

        final BufferedImage image = getBufferedImage(t);

        if (image == null)
            return null;

        // need special grayscale conversion ?
        if (numChannel == 1)
            return Util.getFirstChannelImage(image);

        return IcyBufferedImage.createFrom(image);
    }

    @Override
    public IcyBufferedImage getImage(int serie, int resolution, Rectangle rectangle, int z, int t, int c)
            throws UnsupportedFormatException, IOException
    {
        // video are always single serie image
        if (serie != 0)
            return null;

        IcyBufferedImage result = getImage(z, t);

        // need scaling ?
        if (resolution != 0)
            result = downScale(result, resolution);
        // need clipping ?
        if (rectangle != null)
            result = IcyBufferedImageUtil.getSubImage(result, rectangle, 0, result.getSizeC());
        // need single channel only ?
        if ((c != -1) && (result.getSizeC() > 1))
            result = IcyBufferedImageUtil.extractChannel(result, c);

        return result;
    }

    @Override
    public IcyBufferedImage getThumbnail(int serie) throws UnsupportedFormatException, IOException
    {
        final OMEXMLMetadata meta = getOMEXMLMetaData();
        final int sx = MetaDataUtil.getSizeX(meta, serie);
        final int sy = MetaDataUtil.getSizeY(meta, serie);
        final int st = MetaDataUtil.getSizeT(meta, serie);

        // empty size --> return null
        if ((sx == 0) || (sy == 0) || (st == 0))
            return null;

        final double ratio = Math.min((double) AbstractImageProvider.DEFAULT_THUMBNAIL_SIZE / (double) sx,
                (double) AbstractImageProvider.DEFAULT_THUMBNAIL_SIZE / (double) sy);

        // final thumbnail size
        final int tnx = (int) Math.round(sx * ratio);
        final int tny = (int) Math.round(sy * ratio);
        final int resolution = AbstractImageProvider.getResolutionFactor(sx, sy,
                AbstractImageProvider.DEFAULT_THUMBNAIL_SIZE);

        // don't want to seek to far to find the thumbnail image...
        final IcyBufferedImage result = getImage(serie, resolution, 0, Math.min(st / 2, 50));
        // scale it to desired dimension
        return IcyBufferedImageUtil.scale(result, tnx, tny, FilterType.BILINEAR);
    }

    protected static class XugglerAllFileFilter extends FileFilter
    {
        final static XugglerAllFileFilter instance = new XugglerAllFileFilter();

        @Override
        public boolean accept(File file)
        {
            return Util.canOpenFile(file.getAbsolutePath());
        }

        @Override
        public String getDescription()
        {
            return "All video files / Xuggler";
        }
    }

    protected static class FrameIndex
    {
        int frame;
        long startStamp;
        long endStamp;
        long keyStamp;

        public FrameIndex(int frame, long startStamp, long endStamp, long keyStamp)
        {
            super();

            this.frame = frame;
            this.startStamp = startStamp;
            this.endStamp = endStamp;
            this.keyStamp = keyStamp;
        }

        @Override
        public String toString()
        {
            return "frame: " + frame + "  startStamp: " + startStamp + "  endStamp: " + endStamp + "  keyStamp: "
                    + keyStamp;
        }
    }
}